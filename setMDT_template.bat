REM This script sets location of MDT application share based on Network by
REM pulling the default gateway
REM Author Sean Young (Original Author unknown)
REM Rev 1.0
REM Oct 2019

@echo off

REM Read gateway from WMI and store in %gateway%; Read second octet of %gateway% to determine subnet, change token for different octect
for /f "tokens=2,3 delims={,}" %%a in ('"WMIC NICConfig where IPEnabled="True" get DefaultIPGateway /value | find "I" "') do set Gateway=%%~a
for /f "tokens=2 delims=." %%b in ("%Gateway%") do set DG=%%b

REM 10."1".x.x
If %DG%==1 (set "MDT=\\SERVER\DeploymentShare$\Applications")

REM 10."2".x.x
If %DG%==2 (set "MDT=\\SERVER2\DeploymentShare$\Applications")

REM Repeat as necessary for other networks/subnets