# mdt-application-deployment

These files are used in conjunction with each other to perform Windows applcation deployment via MDT, PDQ, psexec, or any other application deployment system

These files are set up to assume:

*   A server exists on the network
*   The server has a DeploymentShare$ share
*   The following directories exist in the \\\\server\deploymentshare$\ directory
    *   Applications
    *   Scripts
*   The Application setup .msi or .exe is in the \\\\server\deploymentshare$\applications\ directory
*   The following files are in the \\\\server\deploymentshare$\scripts\ directory:
    *   setMDT.bat
    *   check_office_ver.bat
    *   check_os_ver.bat
*   The server has a DeploymentLogs$ share that the deploying user can write to

Once that is set up, the process is fairly straight forward:

Initial Setup:

1.  Edit the setMDT.bat file to reflect your network
2.  Edit the \\\\server\ stock paths in appDeploy_template.bat

Following that:

1.  Open appDeploy_template.bat and change the declaration of variables in the EDIT THESE VARIABLES section
2.  Run the script as admin, or with an account that has permissions to install programs
    *   If your environment is set up as noted above, a log will be created in the \\\\server\deploymentlogs$\\%appname%\ directory stating success or failure

*View the officePluginDeploy_example.bat for an idea of what those changes look like*

Run or deploy the .bat on the target machine, check the \\\\server\deploymentlogs$\\%appName% directory for verification of the install