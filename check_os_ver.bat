REM This script checks the OS Version on a device and stores it in the %OSVer% variable
REM Author Sean Young
REM Rev 1.1
REM Oct 2019

@echo off

REM Retrieve OS bitness
if not "%ProgramFiles(x86)%"=="" (goto query64) else (goto query86)

REM Associate %OSver% with its appropriate name; 64-bit
:query64
REM Windows 10
ver | findstr /i "10.">nul && (set "OSVer=Windows 10 x64")
REM Windows 8.1
ver | findstr /i "6.3">nul && (set "OSVer=Windows 8.1 x64")
REM Windows 8
ver | findstr /i "6.2">nul && (set "OSVer=Windows 8 x64")
REM Windows 7
ver | findstr /i "6.1">nul && (set "OSVer=Windows 7 x64")
REM Windows Vista
ver | findstr /i "6.0">nul && (set "OSVer=Windows Vista x64")
REM Windows XP
ver | findstr /i "5.1">nul && (set "OSVer=Windows XP x64")
exit /b

REM Associate %OSver% with its appropriate name; 32-bit
:query86
REM Windows 10
ver | findstr /i "10.">nul && (set "OSVer=Windows 10 x86")
REM Windows 8.1
ver | findstr /i "6.3">nul && (set "OSVer=Windows 8.1 x86")
REM Windows 8
ver | findstr /i "6.2">nul && (set "OSVer=Windows 8 x86")
REM Windows 7
ver | findstr /i "6.1">nul && (set "OSVer=Windows 7 x86")
REM Windows Vista
ver | findstr /i "6.0">nul && (set "OSVer=Windows Vista x86")
REM Windows XP
ver | findstr /i "5.1">nul && (set "OSVer=Windows XP x86")