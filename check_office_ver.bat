REM This script checks the OS and version of Office that is installed on a device and stores it in the %OfficeVer% variable
REM If Office is not installed, the content of the variable is "Not Installed"
REM Author Sean Young
REM Rev 1.0
REM Sep 2019

@echo off

REM Set default string for OfficeVer variable
set "OfficeVer=Not Installed"

REM Check bitness of Windows by checking for a Program Files (x86) Environment Variable. Only 64-bit Windows will have this.
IF NOT "%ProgramFiles(x86)%"=="" (goto query64) else (goto query86)

REM Operating system is x64. Verify latest version of Office installed
:query64
REM Check for Office 2016 ProPlus 32-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office16.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2016 x86 ProPlus")
REM Check for Office 2013 ProPlus 32-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2013 x86 ProPlus")
REM Check for Office 2013 Standard 32-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.STANDARD
if NOT %errorlevel%==1 (set "OfficeVer=2013 x86 Standard")
REM Check for Office 2010 ProPlus 32-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2010 x86 ProPlus")
REM Check for Office 2010 Standard 32-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.STANDARD
if NOT %errorlevel%==1 (set "OfficeVer=2010 x86 Standard")
REM Check for Office 2016 ProPlus 64-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office16.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2016 x64 ProPlus")
REM Check for Office 2013 ProPlus 64-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2013 x64 Standard")
REM Check for Office 2013 Standard 64-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.STANDARD
if NOT %errorlevel%==1 (set "OfficeVer=2013 x64 Standard")
REM Check for Office 2010 ProPlus 64-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2010 x64 ProPlus")
REM Check for Office 2010 Standard 64-bit installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.STANDARD
if NOT %errorlevel%==1 (set "OfficeVer=2010 x64 Standard")

REM Operating System is x86. Verify latest version of Office installed
:query86
REM Check for Office 2016 ProPlus installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office16.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2016 x86 ProPlus")
REM Check for Office 2013 ProPlus installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2013 x86 ProPlus")
REM Check for Office 2013 Standard installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.STANDARD
if NOT %errorlevel%==1 (set "OfficeVer=2013 x86 Standard")
REM Check for Office 2010 ProPlus installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.PROPLUS
if NOT %errorlevel%==1 (set "OfficeVer=2010 x86 ProPlus")
REM Check for Office 2010 Standard installation
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.STANDARD
if NOT %errorlevel%==1 (set "OfficeVer=2010 x86 Standard")

