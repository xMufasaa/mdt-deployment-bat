REM This is an Application Deployment Template
REM REM Comments are used for actual remarks, :: comments are used for other
REM Author Sean Young
REM Rev 2.4
REM Oct 2019

REM This template should work for a large majority of application deployments just by editing the
REM variables under the EDIT THESE VARIABLES heading in the Setup section. On rare occasions,
REM changes may need to be made to the installation switches under :install86 and :install64.
REM The defaults are "ACCEPT_EULA=1 /quiet /norestart" for .msi installs and "/verysilent" for .exe installs 

@echo off
setlocal

:: *********************************************************************
:: Setup;
:: *********************************************************************
REM Declare Variables

REM If the Z:\ drive is mapped to DeploymentShare, run the setMDT script. If not, use UNC
if exist "Z:\scripts\setMDT.bat" (call Z:\scripts\setMDT.bat)
if defined MDT (goto verifyMDT)
if exist "\\SERVER\deploymentshare$\scripts\setMDT.bat" (call \\SERVER\deploymentshare$\scripts\setMDT.bat)

:verifyMDT
REM Verify the subnet and local MDT path for the device; If %MDT% is not defined, set to local path
REM Edit the "network" line and setMDT.bat to reflect your network; Remember, %DG% is the subnet e.g. 192.168.%DG%.x
echo Network 10.%DG%.x.x
if not defined MDT (set MDT=%~dp0)
echo MDT location %MDT%

REM ########### EDIT THESE VARIABLES ###########
REM Set the application name, spaces are optional; This will be used for creating the log directory
set "appName=Application_Name"

REM Choose the application bitness, if applicable; x86 applications can install on x64 systems, but not vice-versa
REM This is not needed if installing by Office version, as that will dictate the application bitness
REM [Comment one, un-comment other]
set "appBitness=x64"
:: set "appBitness=x86"

REM Choose to Deploy by OS version/bitness or by Office version/bitness
REM Installing by Office version will FAIL if Office is not installed
REM [Comment one, un-comment other]
set "deployBy=OS"
:: set "deployBy=Office"

REM Choose to Install with .msi or .exe
REM [Comment one, un-comment other]
set "installType=msi"
:: set "installType=exe"

REM Set the path to the application's x86 and x64 .msi/.exe, if applicable, including the .msi/.exe itself with no switches
set "installPathx86=%mdt%\App_Folder\App_installer.msi"
set "installPathx64=%mdt%\App_Folder\App_installer.msi"

REM Set local path of the application, this is used to see if it is already installed on the target machine
REM This is likely in %ProgramFiles% or %ProgramFiles(x86)%, but could be elsewhere for non-standard or per-user installs 
set "localPath=%ProgramFiles(x86)%\App_Folder\Launcher.exe"
REM ############################################

REM Set the logFile variable
set "logFile=\\SERVER\DeploymentLogs$\%appName%"

:: *********************************************************************
:: Begin deployment; You shouldn't have to edit anything below this line
:: *********************************************************************

REM Review variables
echo Application Name: %appName%
if %deployBy%=="OS" (if %appBitness%=="x64" (echo Application Installer: %installPathx64%)) ^
else (echo Application Installer: %installPathx86%)
if not %deployBy%=="Office" (echo Application Bitness: %appBitness%)
echo Install by: %deployBy% version
echo Install type: .%installType%
echo Local Install Path: %localPath%
echo Log Location: %logFile%

REM If application folder does not exist in DeploymentLogs$, create it
if not exist %logFile% (mkdir %logFile%)

REM Error-checking; If the program is already installed, skip installation
if exist "%localPath%" (goto SKIP)

REM Read %deployBy% to determine method of install and goto the appropriate one
echo.%deployBy% | findstr /C:"OS">nul && (goto deployByOS) || (goto deployByOffice)

:deployByOS
:: ***** Deployment 1: x86 or x64 deployment by OS version/bitness *****
REM Verify OS version and bitness
call \\SERVER\deploymentshare$\scripts\Check_OS_Ver.bat

REM Read %OSVer% from the above script; if x64, read %appBitness%
echo.%OSVer% | findstr /C:"x86">nul && (goto install86)
echo.%appBitness% | findstr /C:"x64">nul (goto install64) || (goto install86)
:: ******************************************************

:deployByOffice
:: ***** Deployment 2: x86 or x64 deployment by Office version/bitness *****
REM Verify latest version and bitness of Office installed
call \\SERVER\deploymentshare$\scripts\Check_Office_Ver.bat

REM Read %OfficeVer% from the above script
echo.%OfficeVer% | findstr /C:"Not">nul && (goto ERR)
echo.%OfficeVer% | findstr /C:"x64">nul && (goto install64) || (goto install86)
:: **********************************************************

:install86
REM Install x86 Application; Select msi or exe deployment based on %installType%
echo Install Application
echo.%installType% | findstr /C:"msi">nul && ^
call msiexec /i "%installPathx86%" ACCEPT_EULA=1 /quiet /norestart || ^
call "%installPathx86%" /verysilent
if not %errorlevel%==1 (goto DONE) else (goto ERR)

:install64
REM Install x64 Application; Select msi or exe deployment based on %installType%
echo Install Application
echo.%installType% | findstr /C:"msi">nul && ^
call msiexec /i "%installPathx64%" ACCEPT_EULA=1 /quiet /norestart || ^
call "%installPathx64%" /verysilent
if not %errorlevel%==1 (goto DONE) else (goto ERR) 

:: *********************************************************************
:: Cleanup;
:: *********************************************************************
REM Log deployment to %logFile%\log.txt

:SKIP
REM Add entry to log file if application already installed
echo %date% %time% %username% %appName% is already installed on device %computername% >> %logFile%\log.txt
exit /b

:ERR
REM Add entry to log file for failure
echo %date% %time% %username% %appName% was not installed on device %computername%, or another error occurred >> %logFile%\log.txt
exit /b

:DONE
REM Add entry to log file for success
echo %date% %time% %username% %appName% successfully installed on device %computername% >> %logFile%\log.txt

Endlocal